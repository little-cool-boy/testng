package cases;

import com.course.api.AddUser;
import com.course.model.AddUserCase;
import com.course.model.User;
import com.course.utils.DatabaseUtil;
import org.apache.ibatis.session.SqlSession;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 17:13
 */
public class AddUserTest {

    @Test(groups = "addUser", description = "添加用户成功接口")
    public void addUser() throws IOException, InterruptedException {
        SqlSession sqlSession = DatabaseUtil.getSqlSession();
        AddUserCase addUserCase = sqlSession.selectOne("addUserCase", 1);
        System.out.println(addUserCase);
        //发请求获取结果
        String result = AddUser.addUser(addUserCase);
        //验证结果
        Thread.sleep(1000);
        Assert.assertEquals(addUserCase.getExpected(), result);
    }
}
