package cases;

import com.course.api.Login;
import com.course.model.LoginCase;
import com.course.utils.DatabaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 18:00
 */
@Slf4j
public class LoginTest {


    @Test(groups = "loginTrue", description = "用户成功登陆接口")
    public void loginTrue() throws IOException {
        SqlSession session = DatabaseUtil.getSqlSession();
        LoginCase loginCase = session.selectOne("loginCase", 1);

        String s = Login.login(loginCase.getUserName(), loginCase.getPassWord());
        String expected = loginCase.getExpected();
        log.info("expected:{}",expected);
        Assert.assertEquals(expected,s);
    }
}
