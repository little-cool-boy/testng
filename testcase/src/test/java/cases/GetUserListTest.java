package cases;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.course.api.AddUser;
import com.course.api.GetUserList;
import com.course.model.AddUserCase;
import com.course.model.GetUserListCase;
import com.course.model.User;
import com.course.utils.DatabaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 17:52
 */
@Slf4j
public class GetUserListTest {


    @Test(groups = "getUserList", description = "查询用户列表")
    public void getUserList() throws IOException, InterruptedException {
        SqlSession sqlSession = DatabaseUtil.getSqlSession();
        GetUserListCase getUserListCase = sqlSession.selectOne("getUserListCase", 1);

        //发请求获取结果
        JSONArray userList = GetUserList.getUserList();
        log.info("result:{}", userList);
        //验证结果
        Thread.sleep(1000);
        List<User> listUser = sqlSession.selectList(getUserListCase.getExpected(), getUserListCase);
        JSONArray userArray = JSONArray.parseArray(JSON.toJSONString(listUser));
        log.info("expected:{}", userArray);

        Assert.assertEquals(userList.size(), userArray.size());
        for (int i = 0; i < userList.size(); i++) {
            JSONObject actual = (JSONObject) userList.get(i);
            JSONObject expect = (JSONObject) userArray.get(i);
            Assert.assertEquals(actual.toJSONString(),expect.toJSONString());
        }
    }
}
