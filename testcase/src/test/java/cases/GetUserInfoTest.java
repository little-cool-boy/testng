package cases;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.course.api.GetUserInfo;
import com.course.model.GetUserInfoCase;
import com.course.model.User;
import com.course.utils.DatabaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 18:17
 */

@Slf4j
public class GetUserInfoTest {

    @Test(groups = "getUserInfo", description = "查询用户")
    public void getUserInfo() throws IOException, InterruptedException {
        SqlSession sqlSession = DatabaseUtil.getSqlSession();
        GetUserInfoCase getUserInfoCase = sqlSession.selectOne("getUserInfoCase", 1);

        //发送请求
        JSONArray userInfo = GetUserInfo.getUserInfo(getUserInfoCase.getUserId());
        log.info("request:{}", userInfo);

        //验证结果
        Thread.sleep(1000);
        User user = sqlSession.selectOne(getUserInfoCase.getExpected(), getUserInfoCase);

        ArrayList<Object> list = new ArrayList<>();
        list.add(user);

        JSONArray userArray = JSONArray.parseArray(JSON.toJSONString(list));
        log.info("expected:{}", userArray);
        Assert.assertEquals(userArray,userInfo);
    }
}
