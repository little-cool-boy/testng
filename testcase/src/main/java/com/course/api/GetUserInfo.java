package com.course.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.course.model.InferFaceName;
import com.course.utils.ConfigFileUtil;
import com.course.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 18:12
 */
@Slf4j
public class GetUserInfo {


    public static JSONArray getUserInfo(String id) {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id);
        String s = HttpClientUtil.doGet(ConfigFileUtil.getUrl(InferFaceName.GETUSERINFO), map);
        log.info("request:{}",s);
        ArrayList<String> list = new ArrayList<>();
        list.add(s);
        return JSON.parseArray(String.valueOf(list));
    }
}
