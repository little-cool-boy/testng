package com.course.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.course.model.GetUserListCase;
import com.course.model.InferFaceName;
import com.course.utils.ConfigFileUtil;
import com.course.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 17:47
 */
@Slf4j
public class GetUserList {

    public static JSONArray getUserList() {
        String s = HttpClientUtil.doGet(ConfigFileUtil.getUrl(InferFaceName.GETUSERLIST));
        log.info("request:{}",s);
        return JSON.parseArray(s);
    }

}
