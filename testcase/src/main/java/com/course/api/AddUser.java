package com.course.api;

import com.course.config.TestConfig;
import com.course.model.AddUserCase;
import com.course.model.InferFaceName;
import com.course.utils.ConfigFileUtil;
import com.course.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 17:20
 */
@Slf4j
public class AddUser {


    public static String addUser(AddUserCase addUserCase) {
        HashMap<String, String> map = new HashMap<>();
        map.put("username", addUserCase.getUserName());
        map.put("password", addUserCase.getPassWord());

        log.info("request:{}",map);
        String s = HttpClientUtil.doPost(ConfigFileUtil.getUrl(InferFaceName.ADDUSER), map);
        return s;
    }


}
