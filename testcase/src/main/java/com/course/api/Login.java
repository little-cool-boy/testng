package com.course.api;

import com.course.model.InferFaceName;
import com.course.utils.ConfigFileUtil;
import com.course.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/1 19:26
 */
@Slf4j
public class Login {

    public static String login(String username, String password) {
        HashMap<String, String> map = new HashMap<>();
        map.put("username", username);
        map.put("password", password);

        String s = HttpClientUtil.doPost(ConfigFileUtil.getUrl(InferFaceName.LOGIN), map);
        log.info("response:{}", s);
        return s;
    }
}
