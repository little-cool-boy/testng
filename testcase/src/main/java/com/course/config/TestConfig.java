package com.course.config;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 16:25
 */
public class TestConfig {

    public static String loginUrl;
    public static String addUserUrl;
    public static String getUserListUrl;
    public static String getUserInfoUrl;

}
