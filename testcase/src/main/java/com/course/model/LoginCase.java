package com.course.model;

import lombok.Data;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 16:20
 */
@Data
public class LoginCase {
    private Integer id;
    private String userName;
    private String passWord;
    private String expected;
}
