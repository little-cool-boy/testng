package com.course.model;

import lombok.Data;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 16:15
 */
@Data
public class AddUserCase {

    private Integer id;
    private String userName;
    private String passWord;
    private String expected;

}
