package com.course.model;

import lombok.Data;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 16:04
 */
@Data
public class User {

    private Integer id;
    private String username;
    private String password;
}
