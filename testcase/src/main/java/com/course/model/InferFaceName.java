package com.course.model;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 16:22
 */
public enum InferFaceName {

    GETUSERINFO, LOGIN, ADDUSER, GETUSERLIST

}
