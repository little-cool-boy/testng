package com.course.utils;

import com.course.model.InferFaceName;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/5/31 16:49
 */
public class ConfigFileUtil {

    private static ResourceBundle bundle = ResourceBundle.getBundle("application", Locale.CHINA);


    /**
     * 获取url
     *
     * @param name
     * @return
     */
    public static String getUrl(InferFaceName name) {
        String address = bundle.getString("test.url");
        String uri = "";

        if (name == InferFaceName.ADDUSER) {
            uri = bundle.getString("addUser.uri");
        }

        if (name == InferFaceName.GETUSERINFO) {
            uri = bundle.getString("getUserInfo.uri");
        }

        if (name == InferFaceName.GETUSERLIST) {
            uri = bundle.getString("getUserList.uri");
        }

        if (name == InferFaceName.LOGIN) {
            uri = bundle.getString("login.uri");
        }


        String testUtl = address + uri;
        return testUtl;
    }

}
