package com.course.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @author aobo.zhong@woqutech.com
 * @version 1.0
 * @date 2021/6/7 14:12
 */
public class WeChatRobot {


    public static void main(String[] args) {
        JSONObject js = new JSONObject();
        JSONObject jsMarkdown = new JSONObject();
        jsMarkdown.put("content", "测试监控报警");

        jsMarkdown.put("content", "#### 任务名称: " + "用户登录测试" + "\n\n>" + "执行器名称: " + "执行器01" + "\n\n>" + "执行器ip: " + "127.0.0.1" + "\n\n>" + "任务参数: " + "{username:zab}" + "\n\n>" + "###### 执行时间: " + "2021/06/07" + "\n\n>");
        js.put("msgtype", "markdown");
        js.put("markdown", jsMarkdown);

        String s = HttpClientUtil.doPostJson("https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=d641bd9e-43fb-4709-8d30-2697aa858652", JSON.toJSONString(js));
        System.out.println(s);
    }
}
